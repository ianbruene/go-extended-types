package go_extended_types

type Uint128 struct {
	lo uint64
	hi uint64
}

func (i Uint128)Add(other Uint128) (result Uint128) {
	var carry bool
	result = Uint128{}
	result.lo = i.lo + other.lo
	// If it overflowed the result will be less than either addend
	carry = result.lo < i.lo
	result.hi = i.hi + other.hi
	if carry == true {
		result.hi++
	}
	return result
}

func (i Uint128)Subtract(other Uint128) (result Uint128) {
	var borrow bool
	result = Uint128{}
	result.lo = i.lo - other.lo
	borrow = result.lo > i.lo
	result.hi = i.hi - other.hi
	if borrow == true {
		result.hi--
	}
	return result
}

func (i Uint128)Multply(other Uint128) (result Uint128) {
	return Uint128{}
}

func (i Uint128)Divide(other Uint128) (result Uint128) {
	return Uint128{}
}

package go_extended_types

import (
	"testing"
)

func TestAdd_uint128(t *testing.T) {
	// Test basic add
	a := Uint128{lo:0x11, hi:0x22}
	b := Uint128{lo:0x33, hi:0x44}
	c := a.Add(b)
	if c.lo != 0x44 || c.hi != 0x66 {
		t.Fatal("uint128: Basic add test failed:", c)
	}
	// Test add zero
	a.lo = 5
	a.hi = 0
	b.lo = 0
	b.hi = 0
	c = a.Add(b)
	if c.lo != 5 || c.hi != 0 {
		t.Fatal("uint128: Add zero test failed:", c)
	}
	// Test carrying add
	a.lo = 0xFFFFFFFFFFFFFFFF
	a.hi = 0
	b.lo = 0x0000000000000001
	b.hi = 0
	c = a.Add(b)
	if c.lo != 0 || c.hi != 1 {
		t.Fatal("uint128: Carry add test failed:", c)
	}
	// Test overflow add
	a.lo = 1
	a.hi = 0xFFFFFFFFFFFFFFFF
	b.lo = 2
	b.hi = 0x1
	c = a.Add(b)
	if c.lo != 3 || c.hi != 0 {
		t.Fatal("uint128: Overflow add test failed:", c)
	}
}

func TestSubtract_uint128(t *testing.T) {
	// Test basic subtract
	a := Uint128{lo:0x55, hi:0x55}
	b := Uint128{lo:0x11, hi:0x22}
	c := a.Subtract(b)
	if c.lo != 0x44 || c.hi != 0x33 {
		t.Fatal("uint128: Basic subtract test failed:", c)
	}
	// Test subtract zero
	a.lo = 5
	a.hi = 0
	b.lo = 0
	b.hi = 0
	c = a.Subtract(b)
	if c.lo != 5 || c.hi != 0 {
		t.Fatal("uint128: Subtract zero test failed:", c)
	}
	// Test borrow
	a.lo = 0
	a.hi = 1
	b.lo = 0xFFFFFFFFFFFFFFFF
	b.hi = 0
	c = a.Subtract(b)
	if c.lo != 1 || c.hi != 0 {
		t.Fatal("uint128: Subtract borrow test failed:", c)
	}
	// Test underflow
	a.lo = 1
	a.hi = 0
	b.lo = 2
	b.hi = 0
	c = a.Subtract(b)
	if c.lo != 0xFFFFFFFFFFFFFFFF || c.hi != 0xFFFFFFFFFFFFFFFF {
		t.Fatal("uint128: Subtract underflow test failed:", c)
	}
}
